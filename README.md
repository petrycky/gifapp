# gifapp
Projekt powstał w odpowiedzi na ./zadanie.txt.


Jest to aplikacja do wyszukiwania gifów za pomocą api giphy.
Posiada opcję dodawania gifów do ulubionych.
Gify zapisują się w Local Storage.

CSSami się zbytnio nie przejmowałem (jak zostało przykazane w zadaniu), ale łatwiej mi było wrzucić parę gotowych klas, dlatego skorzystałem z UIkit'a :)

Bonus! Dodałem parę rzeczy od siebie takich jak:
* Podczas otwarcia aplikacji wyświetlane jest lista gif - trendów zamiast pustego ekranu
* Dodałem zaraz obok przycisku 'szukaj' ikonkę ładowania która jest widoczna dokładnie tyle ile trwa wysłanie query i odbiór odpowiedzi z api giphy

Z rzeczy które zostały do zrobienia:
* Dopisanie komentarzy i wyjaśnienie kodu, choć powinen być self-explanatory :)
* Poprawienie konfoguracji ES-linta
* Porządne wystylowanie aplikacji

***
Biblioteki użyte w projekcie: 
* Vue, 
* Vuex, 
* Axios, 
* VuexPersist, 
* UIkit
 
***
Podziękowania dla J.S. za znalezienie buga i zaproponowanie bardziej eleganckiego rozwiązania :)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

